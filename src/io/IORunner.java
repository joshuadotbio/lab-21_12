package io;

import java.util.ArrayList;

public class IORunner {

	
	public static void main(String[] args) {
		// edit as necessary
		//testPeople();
		testPhoneNums();
		
	}
	

	
	
	public static void testPeople() {
		// fill in as necessary
		People people = new People();
		ArrayList<Person> list = people.readFile();
		people.printPeople(list);
	}
	
	public static void testPhoneNums() {
		PhoneNums pn = new PhoneNums();
		for (String str : pn.readPhoneNumbers()) {
			System.out.print(str);
		}
	}

	
	
}
